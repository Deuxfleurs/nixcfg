# Onboarding / quick start for new administrators

## How to welcome a new administrator

Give them access to the secrets repo. See: https://guide.deuxfleurs.fr/operations/pass/

Basically:
  - The new administrator generates a GPG key and publishes it on Gitea
  - All existing administrators pull their key and sign it
  - An existing administrator reencrypt the keystore with this new key and push it
  - The new administrator clone the repo and check that they can decrypt the secrets
  - Finally, the new administrator must choose a password to operate over SSH with `./passwd prod rick` where `rick` is the target username


## How to operate a node (connect to Nomad and Consul)

Edit your `~/.ssh/config` file with content such as the following:

```
# Deuxfleurs prod
Host *.machine.deuxfleurs.fr
    IdentityFile ~/.ssh/deuxfleurs_ed25519
    User patapouf
    LocalForward 14646 127.0.0.1:4646
    LocalForward 8501 127.0.0.1:8501
    LocalForward 1389 bottin.service.prod.consul:389
    LocalForward 5432 psql-proxy.service.prod.consul:5432

# Deuxfleurs staging
Host *.machine.staging.deuxfleurs.org
    IdentityFile ~/.ssh/deuxfleurs_ed25519
    User patapouf
    LocalForward 14646 127.0.0.1:4646
    LocalForward 8501 127.0.0.1:8501
    LocalForward 1389 bottin.service.prod.consul:389
    LocalForward 5432 psql-proxy.service.prod.consul:5432
```

Run the TLS proxy and leave the command running:

```bash
# Either staging
./tlsproxy staging
# Or prod
./tlsproxy prod
```

Then connect over SSH to one of the machine with:

```bash
# staging
ssh piranha.machine.staging.deuxfleurs.org
# prod
ssh pasteque.machine.deuxfleurs.fr
```

Tip: you can use `<TAB>` to auto-complete the name of the machine to connect to.

Finally you should see be able to access the production Nomad and Consul by browsing: 

 - Consul: http://localhost:8500
 - Nomad: http://localhost:4646
