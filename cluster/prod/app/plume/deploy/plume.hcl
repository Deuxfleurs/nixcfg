job "plume-blog" {
  datacenters = ["corrin", "neptune", "scorpio"]
  type = "service"

  group "plume" {
    count = 1

    network {
      port "back_port" { }
      port "cache_port" { }
    }

    task "varnish" {
      driver = "docker"
      config {
        image = "varnish:7.6.1"
        network_mode = "host"
        ports = [ "cache_port" ]

	# cache
	mount {
          type = "tmpfs"
          target = "/var/lib/varnish/varnishd:exec"
          readonly = false
          tmpfs_options {
              size = 2684354559 # 2.5GB in bytes
          }
        }
      }

      env {
        VARNISH_SIZE = "2G"
        VARNISH_BACKEND_HOST = "localhost"
        VARNISH_BACKEND_PORT = "${NOMAD_PORT_back_port}"
        VARNISH_HTTP_PORT = "${NOMAD_PORT_cache_port}"
      }

      service {
        name = "plume-cache"
        tags = [
          "plume",
          "tricot plume.deuxfleurs.fr",
          "d53-cname plume.deuxfleurs.fr",
        ]
        port = "cache_port"
        address_mode = "host"
      }
    }

    task "plume" {
      driver = "docker"
      config {
        image = "lxpz/plume_s3:v1"
        network_mode = "host"
        ports = [ "back_port" ]
        command = "sh"
        args = [ "-c", "plm search init; plume" ]
      }

      template {
        data = file("../config/app.env")
        destination = "secrets/app.env"
        env = true
      }

      resources {
        memory = 1024
        memory_max = 1024
        cpu = 100
      }

      service {
        name = "plume-back"
        tags = [
          "plume",
        ]
        port = "back_port"
        address_mode = "host"
        check {
          type = "http"
          protocol = "http"
          port = "back_port"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      }
      restart {
        interval = "20m"
        attempts = 20
        delay    = "15s"
        mode     = "delay"
      }
    }
  }
}

