job "email-android7" {
  # Should not run on the same site as email.hcl (port conflict in diplonat)
  datacenters = ["scorpio", "bespin"]
  type = "service"
  priority = 100

  group "rsa-ecc-proxy" {
    network {
      port "smtps" {
        static = 465
        to = 465
      }
      port "imaps" {
        static = 993
        to = 993
      }
    } 
    task "imaps-proxy" {
      driver = "docker"
      config {
        image = "alpine/socat:1.7.4.4"
        readonly_rootfs = true
        ports = [ "imaps" ]
        network_mode = "host"
        args = [
          "openssl-listen:993,reuseaddr,fork,verify=0,bind=0.0.0.0,cert=/var/secrets/rsa.crt,key=/var/secrets/rsa.key",
          "openssl:imap.deuxfleurs.fr:993,verify=0",
        ]
        volumes = [
          "secrets/certs:/var/secrets"
        ]
      }

      template {
        data = "{{ key \"secrets/email/tls-tls-proxy/rsa.crt\" }}"
        destination = "secrets/certs/rsa.crt"
      }
      template {
        data = "{{ key \"secrets/email/tls-tls-proxy/rsa.key\" }}"
        destination = "secrets/certs/rsa.key"
      }

      resources {
        cpu = 50
        memory = 200
      }

      service {
        name = "imap-android7"
        port = "imaps"
        address_mode = "host"
        tags = [
          "rsa-ecc-proxy",
          "(diplonat (tcp_port 993))",
          "d53-a imap-android7.deuxfleurs.fr",
          # ipv6 is commented for now as socat does not listen on ipv6 now
          # "d53-aaaa imap-android7.deuxfleurs.fr"
        ]
        check {
          type = "tcp"
          port = "imaps"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
    task "smtps-proxy" {
      driver = "docker"
      config {
        image = "alpine/socat:1.7.4.4"
        readonly_rootfs = true
        network_mode = "host"
        ports = [ "smtps" ]
        args = [
          "openssl-listen:465,reuseaddr,fork,verify=0,bind=0.0.0.0,cert=/var/secrets/rsa.crt,key=/var/secrets/rsa.key",
          "openssl:smtp.deuxfleurs.fr:465,verify=0",
        ]
        volumes = [
          "secrets/certs:/var/secrets"
        ]
      }

      template {
        data = "{{ key \"secrets/email/tls-tls-proxy/rsa.crt\" }}"
        destination = "secrets/certs/rsa.crt"
      }
      template {
        data = "{{ key \"secrets/email/tls-tls-proxy/rsa.key\" }}"
        destination = "secrets/certs/rsa.key"
      }

      resources {
        cpu = 50
        memory = 200
      }

      service {
        name = "smtp-android7"
        port = "smtps"
        address_mode = "host"
        tags = [
          "rsa-ecc-proxy",
          "(diplonat (tcp_port 465))",
          "d53-a smtp-android7.deuxfleurs.fr",
          # ipv6 is commented for now as socat does not listen on ipv6 now
          # "d53-aaaa smtp-android7.deuxfleurs.fr"
        ]
        check {
          type = "tcp"
          port = "smtps"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}
