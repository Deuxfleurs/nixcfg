```
sudo docker build -t superboum/amd64_postfix:v1 .
```

```
sudo docker run -t -i \
  -e TLSINFO="/C=FR/ST=Bretagne/L=Rennes/O=Deuxfleurs/CN=smtp.deuxfleurs.fr" \
  -e MAILNAME="smtp.deuxfleurs.fr" \
  -p 25:25 \
  -p 465:465 \
  -p 587:587 \
  -v `pwd`/../../ansible/roles/container_conf/files/email/postfix-conf:/etc/postfix-conf \
  -v /mnt/glusterfs/email/postfix-ssl/private:/etc/ssl/private \
  -v /mnt/glusterfs/email/postfix-ssl/certs:/etc/ssl/certs \
  superboum/amd64_postfix:v1 \
  bash
```

