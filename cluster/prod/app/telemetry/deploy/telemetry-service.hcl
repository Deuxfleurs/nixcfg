job "telemetry-service" {
  datacenters = ["corrin", "scorpio", "dathormir"]
  type = "service"

  group "grafana" {
    count = 1

    network {
      port "grafana" {
        static = 3719
      }
    }

    task "restore-db" {
      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      driver = "docker"
      config {
        image = "litestream/litestream:0.3.13"
        args = [
          "restore", "-config", "/etc/litestream.yml", "/ephemeral/grafana.db"
        ]
        volumes = [
          "../alloc/data:/ephemeral",
          "secrets/litestream.yml:/etc/litestream.yml"
        ]
      }
      user = "472"

      template {
        data = file("../config/grafana-litestream.yml")
        destination = "secrets/litestream.yml"
      }

      resources {
        memory = 50
        memory_max = 200
        cpu = 100
      }
    }

    task "grafana" {
      driver = "docker"
      config {
        image = "grafana/grafana:11.4.1"
        network_mode = "host"
        ports = [ "grafana" ]
        volumes = [
          "../alloc/data:/var/lib/grafana",
          "secrets/prometheus.yaml:/etc/grafana/provisioning/datasources/prometheus.yaml",
          "secrets/ldap.toml:/etc/grafana/ldap.toml"
        ]
      }

      template {
        data = file("../config/grafana-datasource-prometheus.yaml")
        destination = "secrets/prometheus.yaml"
      }

      template {
        data = file("../config/grafana-ldap.toml")
        destination = "secrets/ldap.toml"
      }

      template {
        data = <<EOH
GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource,grafana-piechart-panel,grafana-worldmap-panel,grafana-polystat-panel
GF_SERVER_HTTP_PORT=3719
GF_AUTH_LDAP_ENABLED=true
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 200
        memory_max = 400
        cpu = 300
      }

      service {
        tags = [
            "grafana",
            "tricot grafana.deuxfleurs.fr",
            "d53-cname grafana.deuxfleurs.fr",
        ]
        port = 3719
        address_mode = "driver"
        name = "grafana"
        check {
          type = "tcp"
          port = 3719
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }

    task "replicate-db" {
      driver = "docker"
      config {
        image = "litestream/litestream:0.3.13"
        args = [
          "replicate", "-config", "/etc/litestream.yml"
        ]
        volumes = [
          "../alloc/data:/ephemeral",
          "secrets/litestream.yml:/etc/litestream.yml"
        ]
      }
      user = "472"

      template {
        data = file("../config/grafana-litestream.yml")
        destination = "secrets/litestream.yml"
      }

      resources {
        memory = 50
        memory_max = 200
        cpu = 100
      }
    }
  }
}
