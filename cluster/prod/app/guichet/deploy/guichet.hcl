job "guichet" {
  datacenters = ["corrin", "neptune", "scorpio"]
  type = "service"
  priority = 90

  group "guichet" {
    count = 1

    network {
      port "web_port" { to = 9991 }
    }

    task "guichet" {
      driver = "docker"
      config {
        image = "dxflrs/guichet:0x4y7bj1qb8w8hckvpbzlgyxh63j66ij"
        args = [ "server", "-config", "/etc/config.json" ]
        readonly_rootfs = true
        ports = [ "web_port" ]
        volumes = [
          "secrets/config.json:/etc/config.json"
        ]
      }

      template {
        data = file("../config/guichet/config.json.tpl")
        destination = "secrets/config.json"
      }

      resources {
        # limite de mémoire un peu élevée par précaution.
        # avec 200M, j'ai observé guichet se faire OOM-killed au moment
        # où un nouvel utilisateur clique sur un lien d'invitation
        # fraichement généré.
        memory = 300
      }

      service {
        name = "guichet"
        tags = [
          "guichet",
          "tricot guichet.deuxfleurs.fr",
          "d53-cname guichet.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

