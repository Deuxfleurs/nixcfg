## Install Debian

We recommend Debian Bullseye

## Install Docker CE from docker.io

Do not use the docker engine shipped by Debian

Doc:

  - https://docs.docker.com/engine/install/debian/
  - https://docs.docker.com/compose/install/

On a fresh install, as root:

```bash
# Remove all pre-existing packages
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do apt-get remove $pkg; done

# Add Docker's official GPG key:
apt-get update
apt-get install ca-certificates curl
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
      tee /etc/apt/sources.list.d/docker.list > /dev/null
      apt-get update

# Install Docker
apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

## Install the runner

```bash
wget https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/raw/branch/main/cluster/prod/app/woodpecker-ci/integration/nix.conf
wget https://git.deuxfleurs.fr/Deuxfleurs/nixcfg/raw/branch/main/cluster/prod/app/woodpecker-ci/integration/docker-compose.yml
```

Create a new Agent for your runner in the Woodpecker admin, and copy the agent secret value.

Edit `docker-compose.yml` and insert your agent secret as the value for WOODPECKER_AGENT_SECRET.
Update other values including hostname and max workflows for your runner.

```bash
COMPOSE_PROJECT_NAME=woodpecker docker-compose up -d
```

That's all folks.
