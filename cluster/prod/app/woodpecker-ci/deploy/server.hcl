job "woodpecker-ci" {
  datacenters = ["corrin", "scorpio"]
  type = "service"

  group "server" {
    count = 1

    network {
      port "web_port" {
        static = 14080
        to = 14080
      }
      port "grpc_port" {
        static = 14090
        to = 14090
      }
      port "grpc_tls_port" {
        static = 14453
        to = 14453
      }
    }

    task "server" {
      driver = "docker"
      config {
        image = "woodpeckerci/woodpecker-server:v3.0.1"
        ports = [ "web_port", "grpc_port" ]
        network_mode = "host"
      }

      template {
        data = <<EOH
WOODPECKER_OPEN=true
WOODPECKER_ORGS=Deuxfleurs,distorsion
WOODPECKER_ADMIN=lx

WOODPECKER_HOST=https://woodpecker.deuxfleurs.fr
WOODPECKER_AGENT_SECRET={{ key "secrets/woodpecker-ci/agent_secret" }}

# secret encryption is broken in woodpecker currently
# WOODPECKER_ENCRYPTION_KEY={{ key "secrets/woodpecker-ci/secrets_encryption_key" }}

WOODPECKER_SERVER_ADDR=[::]:14080
WOODPECKER_GRPC_ADDR=[::]:14090
# WOODPECKER_GRPC_SECRET={{ key "secrets/woodpecker-ci/grpc_secret" }}

WOODPECKER_DATABASE_DRIVER=postgres
WOODPECKER_DATABASE_DATASOURCE=postgres://woodpecker:{{ key "secrets/woodpecker-ci/db_password" | trimSpace }}@{{ env "meta.site" }}.psql-proxy.service.prod.consul:5432/woodpecker?sslmode=disable

WOODPECKER_GITEA=true
WOODPECKER_GITEA_URL=https://git.deuxfleurs.fr
WOODPECKER_GITEA_CLIENT={{ key "secrets/woodpecker-ci/oauth_client_id" }}
WOODPECKER_GITEA_SECRET={{ key "secrets/woodpecker-ci/oauth_client_secret" }}

WOODPECKER_LOG_LEVEL=debug
WOODPECKER_ENVIRONMENT=NIX_REMOTE:daemon
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        cpu = 100
        memory = 200
      }

      service {
        name = "woodpecker"
        tags = [
          "woodpecker",
          "tricot woodpecker.deuxfleurs.fr",
          "d53-cname woodpecker.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
      /*
        check {
          type = "http"
          protocol = "http"
          port = "web_port"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      */
      }
      service {
        name = "woodpecker-grpc"
        tags = [
          "woodpecker-grpc",
          # The tricot tag is necessary for tricot to get us a tls certificate,
          # but it will not make the grpc endpoint work as tricot cannot
          # proxy grpc traffic by itself.
          "tricot woodpecker-grpc.deuxfleurs.fr",
        ]
        port = "grpc_port"
        address_mode = "host"
      }
    }

    task "grpc_tls" {
      driver = "docker"
      config {
        image = "nginx:1.25.3"
        ports = [ "grpc_tls_port" ]
        volumes = [
          "secrets/ssl/certs:/etc/ssl/certs",
          "secrets/ssl/private:/etc/ssl/private",
          "secrets/conf/:/etc/nginx/",
        ]
        network_mode = "host"
      }

      template {
        data = <<EOH
events {}
http {
  server {
    listen 0.0.0.0:14453 ssl;
    listen [::]:14453 ssl;
    http2 on;
    server_name woodpecker-grpc.deuxfleurs.fr;
    resolver 127.0.0.1 valid=30s;

    ssl_certificate "/etc/ssl/certs/woodpecker.cert";
    ssl_certificate_key "/etc/ssl/certs/woodpecker.key";

    location / {
        grpc_pass grpc://woodpecker-grpc.service.prod.consul:14090;
        grpc_read_timeout 1800s;
        grpc_send_timeout 1800s;
    }
  }
}
EOH
        destination = "secrets/conf/nginx.conf"
      }

      template {
        data = "{{ with $d := key \"tricot/certs/woodpecker-grpc.deuxfleurs.fr\" | parseJSON }}{{ $d.key_pem }}{{ end }}"
        destination = "secrets/ssl/certs/woodpecker.key"
      }
      template {
        data = "{{ with $d := key \"tricot/certs/woodpecker-grpc.deuxfleurs.fr\" | parseJSON }}{{ $d.cert_pem }}{{ end }}"
        destination = "secrets/ssl/certs/woodpecker.cert"
      }

      service {
        name = "woodpecker-grpc-tls"
        tags = [
          "woodpecker-grpc-tls",
      "d53-a woodpecker-grpc.deuxfleurs.fr",
      "d53-aaaa woodpecker-grpc.deuxfleurs.fr",
          "(diplonat (tcp_port 14453))"
        ]
        port = "grpc_tls_port"
        address_mode = "host"
      }
    }
  }
}
