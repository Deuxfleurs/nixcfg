## Pour remonter locement un backup de PSQL fait par Nomad (backup-weekly.hcl)

```bash
export AWS_BUCKET=backups-pgbasebackup
export AWS_ENDPOINT=s3.deuxfleurs.shirokumo.net
export AWS_ACCESS_KEY_ID=$(consul kv get "secrets/postgres/backup/aws_access_key_id")
export AWS_SECRET_ACCESS_KEY=$(consul kv get secrets/postgres/backup/aws_secret_access_key)
export CRYPT_PUBLIC_KEY=$(consul kv get secrets/postgres/backup/crypt_public_key)
```

Et voilà le travail : 

```bash
$ aws s3 --endpoint https://$AWS_ENDPOINT ls
2022-04-14 17:00:50 backups-pgbasebackup

$ aws s3 --endpoint https://$AWS_ENDPOINT ls s3://backups-pgbasebackup
                           PRE 2024-07-28 00:00:36.140539/
                           PRE 2024-08-04 00:00:21.291551/
                           PRE 2024-08-11 00:00:26.589762/
                           PRE 2024-08-18 00:00:40.873939/
                           PRE 2024-08-25 01:03:54.672763/
                           PRE 2024-09-01 00:00:20.019605/
                           PRE 2024-09-08 00:00:16.969740/
                           PRE 2024-09-15 00:00:37.951459/
                           PRE 2024-09-22 00:00:21.030452/

$ aws s3 --endpoint https://$AWS_ENDPOINT ls "s3://backups-pgbasebackup/2024-09-22 00:00:21.030452/"
2024-09-22 03:23:28     623490 backup_manifest
2024-09-22 03:25:32 6037121487 base.tar.gz
2024-09-22 03:25:33   19948939 pg_wal.tar.gz
```
