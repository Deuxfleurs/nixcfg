job "cms" {
  datacenters = ["corrin", "neptune", "scorpio"]
  type = "service"

  priority = 100

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "auth" {
    count = 1

    network {
      port "web_port" { }
    }

    task "teabag" {
      driver = "docker"
      config {
        # Using a digest to pin the container as no tag is provided
        # https://github.com/denyskon/teabag/pkgs/container/teabag
        image = "ghcr.io/denyskon/teabag@sha256:d5af7c6caf172727fbfa047c8ee82f9087ef904f0f3bffdeec656be04e9e0a14"
        ports = [ "web_port" ]
        volumes = [
          "secrets/teabag.env:/etc/teabag/teabag.env",
        ]
      }

      template {
        data = file("../config/teabag.env")
        destination = "secrets/teabag.env"
      }

      resources {
        memory = 20
        memory_max = 50
        cpu = 50
      }

      service {
        name = "teabag"
        tags = [
          "teabag",
          "tricot teabag.deuxfleurs.fr",
          "d53-cname teabag.deuxfleurs.fr",
        ]
        port = "web_port"
        check {
          type = "http"
          protocol = "http"
          port = "web_port"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      }

      restart {
        interval = "30m"
        attempts = 20
        delay    = "15s"
        mode     = "delay"
      }
    }
  }
}

