job "coturn" {
  datacenters = ["corrin", "neptune", "scorpio"]
  type = "service"

  priority = 100

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "main" {
    count = 1

    network {
      port "prometheus" { static = 9641 }
      port "turn_ctrl" { static = 3478 }
      port "turn_data0" { static = 49160 }
      port "turn_data1" { static = 49161 }
      port "turn_data2" { static = 49162 }
      port "turn_data3" { static = 49163 }
      port "turn_data4" { static = 49164 }
      port "turn_data5" { static = 49165 }
      port "turn_data6" { static = 49166 }
      port "turn_data7" { static = 49167 }
      port "turn_data8" { static = 49168 }
      port "turn_data9" { static = 49169 }
    }

    task "turnserver" {
      driver = "docker"
      config {
        image = "coturn/coturn:4.6.1-r2-alpine"
        ports = [ "prometheus", "turn_ctrl", "turn_data0", "turn_data1", "turn_data2",
                  "turn_data3", "turn_data4", "turn_data5", "turn_data6", "turn_data7",
                  "turn_data8", "turn_data9" ]
        entrypoint = ["/local/docker-entrypoint.sh"]
        network_mode = "host"
      }

      template {
        data = file("../config/docker-entrypoint.sh")
        destination = "local/docker-entrypoint.sh"
        perms = 555
      }

      resources {
        memory = 20
        memory_max = 50
        cpu = 50
      }

      service {
        name = "coturn"
        tags = [
          "coturn",
          "d53-cname turn.deuxfleurs.fr",
          "(diplonat (tcp_port 3478) (udp_port 3478 49160 49161 49162 49163 49164 49165 49166 49167 49168 49169))",
        ]
        port = "turn_ctrl"
        check {
          type = "http"
          protocol = "http"
          port = "prometheus"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      }

      restart {
        interval = "30m"
        attempts = 20
        delay    = "15s"
        mode     = "delay"
      }
    }
  }
}

