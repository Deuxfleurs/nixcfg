#!/bin/sh

turnserver \
    -n \
    --external-ip=$(detect-external-ip) \
    --min-port=49160 \
    --max-port=49169 \
    --log-file=stdout \
    --use-auth-secret \
    --realm turn.deuxfleurs.fr \
    --no-cli \
    --no-tls \
    --no-dtls \
    --prometheus \
    --static-auth-secret '{{ key "secrets/coturn/static-auth-secret" | trimSpace }}' 
