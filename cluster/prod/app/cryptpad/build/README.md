# CryptPad for NixOS with Deuxfleurs flavour

## Basic Usage

### Building

To build and load the Docker image used in our Deuxfleurs deployment, run:

``` shell
docker load -i $(nix-build deuxfleurs.nix -A docker)
```

### Updating Cryptpad to a newer version

- Check whether the cryptpad build instructions and the `install-onlyoffice.sh`
  script has changed. If yes, then update `default.nix` accordingly.
- In `default.nix`, update the `version` field for cryptpad
- In `default.nix`, change the hash (any change works) of the release and `npmDepsHash` to trigger a rebuild
- Run `nix-build deuxfleurs.nix`. This will fail because the hashes have changed, but tell you the correct hash to insert in `default.nix`. 

## More info

The `default.nix` file follows the nixpkgs `callPackage` convention for fetching dependencies, so you need to either:

- Run `nix-build --expr '{ ... }@args: (import <nixpkgs> {}).callPackage ./default.nix args'`
- Do the `callPackage from a higher-level directory importing your package`

### Docker

The `docker.nix` derives into a Docker image you can load simply by running:

```shell
docker load -i $(nix-build docker.nix)
```

You can then test the built Docker image using the provided `docker-compose.yml` and `config.js` files, which are
configured to render the instance accessible at `http://localhost:3000` with data stored into the `_data` folder.


### Deuxfleurs flavour
The `deuxfleurs.nix` file derives into two derivations: The CryptPad derivation itself and a Docker image,
which can be choose by passing the `-A [name]` flags to `nix-build`

For example, to build and load the Deuxfleurs-flavoured CryptPad Docker image, you run:

```shell
docker load -i $(nix-build deuxfleurs.nix -A docker)
```

## OnlyOffice integration
Apart for `deuxfleurs.nix`, both `default.nix` and `docker.nix` files build CryptPad with a copy of OnlyOffice pre-built and
used by CryptPad, which can result to large Docker image (~2.6GiB)

This behaviour is configurable by passing the `--arg withOnlyOffice false` flag to `nix-build` when building them.

## Updating the Deuxfleurs pinned nixpkgs
The pinned sources files are generated with the [npins](https://github.com/andir/npins) tool.

To update the pinned nixpkgs, you simply run the following command:

```shell
npins update
```

To modify the pinned nixpkgs, remove it and re-add it using the new target, for exemple for `nixos-unstable`:

```shell
npins remove nixpkgs
npins add --name nixpkgs channel nixos-unstable
```
