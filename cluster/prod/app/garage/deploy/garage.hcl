job "garage" {
  datacenters = ["bespin", "scorpio", "corrin"]
  type = "system"
  priority = 80

  group "garage" {
    network {
      port "s3" { static = 3900 }
      port "rpc" { static = 3901 }
      port "web" { static = 3902 }
      port "admin" { static = 3903 }
      port "k2v" { static = 3904 }
    }

    update {
      # When upgrading the service, stop and upgrade nodes one by one.
      # This should allow performing minor upgrades without downtime.
      # (A higher value for max_parallel would risk stopping nodes in different
      #  zones at the same time, which would make the cluster unavailable)
      max_parallel = 1
      min_healthy_time = "30s"
      healthy_deadline = "5m"
    }

    task "server" {
      driver = "docker"
      config {
        image = "dxflrs/garage:v1.99.1-internal"
        command = "/garage"
        args = [ "server" ]
        network_mode = "host"
        volumes = [
          "/mnt/storage/garage/data:/data",
          "/mnt/ssd/garage/meta:/meta",
          "secrets/garage.toml:/etc/garage.toml",
          "secrets:/etc/garage",
        ]
        logging {
          type = "journald"
        }
      }

      template {
        data = file("../config/garage.toml")
        destination = "secrets/garage.toml"
        change_mode = "noop"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "secrets/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "secrets/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "secrets/consul-client.key"
      }

      resources {
        memory = 1000
        memory_max = 3000
        cpu = 1000
      }

      kill_timeout = "20s"

      restart {
        interval = "30m"
        attempts = 10
        delay    = "15s"
        mode     = "delay"
      }

      #### Configuration for service ports: admin port (internal use only)

      service {
        name = "garage-admin"
        port = "admin"
        address_mode = "host"
        # Check that Garage is alive and answering TCP connections
        check {
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      #### Configuration for service ports: externally available ports (S3 API, K2V, web)

      service {
        name = "garage-api"
        tags = [
          "garage_api",
          "tricot garage.deuxfleurs.fr",
          "tricot *.garage.deuxfleurs.fr",
          "tricot-on-demand-tls-ask http://garage-admin.service.prod.consul:3903/check",
          "tricot-site-lb",
        ]
        port = "s3"
        address_mode = "host"
        # Check 1: Garage is alive and answering TCP connections
        check {
          name = "garage-api-live"
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
        # Check 2: Garage is in a healthy state and requests should be routed here
        check {
          name = "garage-api-healthy"
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }

      service {
        name = "garage-k2v"
        tags = [
          "garage_k2v",
          "tricot k2v.deuxfleurs.fr",
          "tricot-site-lb",
        ]
        port = "k2v"
        address_mode = "host"
        # Check 1: Garage is alive and answering TCP connections
        check {
          name = "garage-k2v-live"
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
        # Check 2: Garage is in a healthy state and requests should be routed here
        check {
          name = "garage-k2v-healthy"
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }

      service {
        name = "garage-web"
        tags = [
            "garage-web",
            "tricot * 1",
            "tricot-add-header Strict-Transport-Security max-age=63072000; includeSubDomains; preload",
            "tricot-add-header X-Frame-Options SAMEORIGIN",
            "tricot-add-header X-XSS-Protection 1; mode=block",
            "tricot-add-header X-Content-Type-Options nosniff",
            "tricot-on-demand-tls-ask http://garage-admin.service.prod.consul:3903/check",
            "tricot-site-lb",
        ]
        port = "web"
        address_mode = "host"
        # Check 1: Garage is alive and answering TCP connections
        check {
          name = "garage-web-live"
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
        # Check 2: Garage is in a healthy state and requests should be routed here
        check {
          name = "garage-web-healthy"
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }

      service {
        tags = [
          "garage-redirect-dummy",
          "tricot www.deuxfleurs.fr 2",
          "tricot osuny.org 2",
          "tricot www.degrowth.net 2",
          "tricot-add-redirect www.deuxfleurs.fr deuxfleurs.fr 301",
          "tricot-add-redirect osuny.org www.osuny.org 301",
          "tricot-add-redirect www.degrowth.net degrowth.net 301",
        ]
        name = "garage-redirect-dummy"
        address_mode = "host"
        port = "web"
        on_update = "ignore"
      }
    }
  }
}
