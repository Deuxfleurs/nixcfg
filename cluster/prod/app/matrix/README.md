# Informations relatives à la config Matrix

## Ressources

- La doc de Synapse est là : https://element-hq.github.io/synapse/latest/welcome_and_overview.html

### Métriques 

- La page pour configurer les metrics : https://element-hq.github.io/synapse/latest/usage/configuration/config_documentation.html?highlight=metrics#metrics
- La page pour le tutoriel sur configurer les metrics avec Prometheus : https://element-hq.github.io/synapse/latest/metrics-howto.html?highlight=metrics#how-to-monitor-synapse-metrics-using-prometheus

---

> Avec Nix on n'aurait pas tous ces problèmes.
