job "matrix" {
  datacenters = ["scorpio", "corrin"]
  type = "service"
  priority = 40

  group "matrix" {
    count = 1

    network {
      port "api_port" { static = 8008 }
      port "web_port" { to = 8043 }
    }

    task "synapse" {
      driver = "docker"

      config {
        image = "superboum/amd64_synapse:v61"
        network_mode = "host"
        readonly_rootfs = true
        ports = [ "api_port" ]
        command = "python"
        args = [
          "-m", "synapse.app.homeserver",
          "-n",
          "-c", "/etc/matrix-synapse/homeserver.yaml"
        ]
        volumes = [
          "secrets/conf:/etc/matrix-synapse",
          "/tmp/synapse-media:/var/lib/matrix-synapse/media",
          "/tmp/synapse-uploads:/var/lib/matrix-synapse/uploads",
          "/tmp/synapse-logs:/var/log/matrix-synapse",
          "/tmp/synapse:/tmp"
        ]
      }

      template {
        data = file("../config/synapse/homeserver.yaml")
        destination = "secrets/conf/homeserver.yaml"
      }

      template {
        data = file("../config/synapse/log.yaml")
        destination = "secrets/conf/log.yaml"
      }

      template {
        data = file("../config/synapse/conf.d/server_name.yaml")
        destination = "secrets/conf/server_name.yaml"
      }

      template {
        data = file("../config/synapse/conf.d/report_stats.yaml")
        destination = "secrets/conf/report_stats.yaml"
      }

      # --- secrets ---
      template {
        data = "{{ key \"secrets/chat/synapse/homeserver.signing.key\" }}"
        destination = "secrets/conf/homeserver.signing.key"
      }

      env {
        SYNAPSE_CACHE_FACTOR = 1
      }

      resources {
        cpu = 1000
        memory = 500
        memory_max = 1000
      }

      service {
        name = "synapse"
        port = "api_port"
        address_mode = "host"
        tags = [
          "matrix",
          "tricot im.deuxfleurs.fr/_matrix 100",
          "tricot im.deuxfleurs.fr:443/_matrix 100",
          "tricot im.deuxfleurs.fr/_synapse 100",
          "tricot-add-header Access-Control-Allow-Origin *",
          "d53-cname im.deuxfleurs.fr",
        ]
        check {
          type = "tcp"
          port = "api_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }


    task "media-async-upload" {
      driver = "docker"

      config {
        image = "superboum/amd64_synapse:v61"
        readonly_rootfs = true
        command = "/usr/local/bin/matrix-s3-async"
        work_dir = "/tmp"
        volumes = [
          "/tmp/synapse-media:/var/lib/matrix-synapse/media",
          "/tmp/synapse-uploads:/var/lib/matrix-synapse/uploads",
          "/tmp/synapse:/tmp"
        ]
      }

      resources {
        cpu = 100
        memory = 200
        memory_max = 500
      }

      template {
        data = <<EOH
AWS_ACCESS_KEY_ID={{ key "secrets/chat/synapse/s3_access_key" | trimSpace }}
AWS_SECRET_ACCESS_KEY={{ key "secrets/chat/synapse/s3_secret_key" | trimSpace }}
AWS_DEFAULT_REGION=garage
PG_USER={{ key "secrets/chat/synapse/postgres_user" | trimSpace }}
PG_PASS={{ key "secrets/chat/synapse/postgres_pwd" | trimSpace }}
PG_DB={{ key "secrets/chat/synapse/postgres_db" | trimSpace }}
PG_HOST={{ env "meta.site" }}.psql-proxy.service.prod.consul
PG_PORT=5432
EOH
        destination = "secrets/env"
        env = true
      }
    }

    task "riotweb" {
      driver = "docker"
      config {
        image = "superboum/amd64_elementweb:v37"
        ports = [ "web_port" ]
        volumes = [
          "secrets/config.json:/srv/http/config.json"
        ]
      }

      template {
        data = file("../config/riot_web/config.json")
        destination   = "secrets/config.json"
      }

      resources {
        memory = 21
      }

      service {
        tags = [
          "webstatic",
          "tricot im.deuxfleurs.fr 10",
          "tricot riot.deuxfleurs.fr 10",
          "d53-cname riot.deuxfleurs.fr",
        ]
        port = "web_port"
        address_mode = "host"
        name = "webstatic"
        check {
          type = "tcp"
          port = "web_port"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}

