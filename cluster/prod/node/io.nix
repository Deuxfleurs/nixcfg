{ ... }:
{
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  services.openssh.ports = [ 22 33603 ];

  deuxfleurs.hostName = "io";
  deuxfleurs.staticIPv4.address = "192.168.1.36";
  deuxfleurs.staticIPv6.address = "2a01:e0a:5e4:1d0:52e5:49ff:fe5c:5f35";
}
