# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 5;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "pamplemousse";
  deuxfleurs.staticIPv4.address = "192.168.5.201";
  deuxfleurs.staticIPv6.address = "2001:912:1ac0:2200::201";
  deuxfleurs.isRaftServer = true;
}
