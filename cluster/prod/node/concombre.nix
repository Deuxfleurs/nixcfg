# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "concombre";
  deuxfleurs.staticIPv4.address = "192.168.1.31";
  deuxfleurs.staticIPv6.address = "2001:910:1204:1::31";
}
