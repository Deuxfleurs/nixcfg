{ ... }:
{
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  services.openssh.ports = [ 22 33601 ];

  deuxfleurs.hostName = "onion";
  deuxfleurs.staticIPv4.address = "192.168.1.34";
  deuxfleurs.staticIPv6.address = "2a01:e0a:5e4:1d0:223:24ff:feb0:e866";
}
