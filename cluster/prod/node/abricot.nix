# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "abricot";
  deuxfleurs.staticIPv4.address = "192.168.1.41";
  deuxfleurs.staticIPv6.address = "2a01:e0a:e4:2dd0::41";
}
