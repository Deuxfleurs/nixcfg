{ config, pkgs, ... } @ args:

{
  deuxfleurs.clusterName = "prod";

  # The IP range to use for the Wireguard overlay of this cluster
  deuxfleurs.clusterPrefix = "10.83.0.0/16";

  deuxfleurs.clusterNodes = {
    "df-ykl" = {
      siteName = "bespin";
      publicKey = "bIjxey/VhBgVrLa0FxN/KISOt2XFmQeSh1MPivUq9gg=";
      address = "10.83.3.1";
      endpoint = "109.130.116.21:33731";
    };
    "df-ymf" = {
      siteName = "bespin";
      publicKey = "pUIKv8UBl586O7DBrHBsb9BgNU7WlYQ2r2RSNkD+JAQ=";
      address = "10.83.3.2";
      endpoint = "109.130.116.21:33732";
    };
    "df-ymk" = {
      siteName = "bespin";
      publicKey = "VBmpo15iIJP7250NAsF+ryhZc3j+8TZFnE1Djvn5TXI=";
      address = "10.83.3.3";
      endpoint = "109.130.116.21:33733";
    };
    "abricot" = {
      siteName = "scorpio";
      publicKey = "Sm9cmNZ/BfWVPFflMO+fuyiera4r203b/dKhHTQmBFg=";
      address = "10.83.4.1";
      endpoint = "82.65.41.110:33741";
    };
    "ananas" = {
      siteName = "scorpio";
      publicKey = "YC78bXUaAQ02gz0bApenM4phIo/oMPR78QCmyG0tay4=";
      address = "10.83.4.2";
      endpoint = "82.65.41.110:33742";
    };
    "onion" = {
      siteName = "dathomir";
      publicKey = "gpeqalqAUaYlMuebv3glQeZyE64+OpkyIHFhfStJQA4=";
      address = "10.83.5.1";
      endpoint = "82.64.238.84:33740";
    };
    "oseille" = {
      siteName = "dathomir";
      publicKey = "T87GzAQt02i00iOMbEm7McA/VL9OBrG/kCrgoNh5MmY=";
      address = "10.83.5.2";
      endpoint = "82.64.238.84:33741";
    };
    "io" = {
      siteName = "dathomir";
      publicKey = "3+VvWJtABOAd6zUCMROhqGbNtkQRtoIkVmYn0M81jQw=";
      address = "10.83.5.3";
      endpoint = "82.64.238.84:33742";
    };
    "ortie" = {
      siteName = "dathomir";
      publicKey = "tbx2mvt3TN3Xd+ermwwZ6it80VWT5949cKH9BRFgvzE=";
      address = "10.83.5.4";
      endpoint = "82.64.238.84:33743";
    };
    "pamplemousse" = {
      siteName = "corrin";
      publicKey = "6y5GrNXEql12AObuSfOHGxxUKpdlcyapu+juLYOEBhc=";
      address = "10.83.6.1";
      endpoint = "45.81.62.36:33731";
    };
    "pasteque" = {
      siteName = "corrin";
      publicKey = "7vPq0z6JVxTLEebasUlR5Uu4dAFZxfddhjWtIYhCoXw=";
      address = "10.83.6.2";
      endpoint = "45.81.62.36:33732";
    };
  };

  # Pin Nomad version
  services.nomad.package = pkgs.nomad_1_6;
  nixpkgs.config.allowUnfree = true; # Accept nomad's BSL license

  # Bootstrap IPs for Consul cluster,
  # these are IPs on the Wireguard overlay
  services.consul.extraConfig.retry_join = [
    "10.83.3.1"  # df-ykl
    "10.83.4.2"  # ananas
    "10.83.6.1"  # pamplemousse
  ];

  deuxfleurs.adminAccounts = {
    lx = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIw+IIX8+lZX9RrHAbwi/bncLYStXpI4EmK3AUcqPY2O lx@kusanagi "
    ];
    quentin = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDT1+H08FdUSvdPpPKdcafq4+JRHvFVjfvG5Id97LAoROmFRUb/ZOMTLdNuD7FqvW0Da5CPxIMr8ZxfrFLtpGyuG7qdI030iIRZPlKpBh37epZHaV+l9F4ZwJQMIBO9cuyLPXgsyvM/s7tDtrdK1k7JTf2EVvoirrjSzBaMhAnhi7//to8zvujDtgDZzy6aby75bAaDetlYPBq2brWehtrf9yDDG9WAMYJqp//scje/WmhbRR6eSdim1HaUcWk5+4ZPt8sQJcy8iWxQ4jtgjqTvMOe5v8ZPkxJNBine/ZKoJsv7FzKem00xEH7opzktaGukyEqH0VwOwKhmBiqsX2yN quentin@dufour.io"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBu+KUebaWwlugMC5fGbNhHc6IaQDAC6+1vMc4Ww7nVU1rs2nwI7L5qcWxOwNdhFaorZQZy/fJuCWdFbF61RCKGayBWPLZHGPsfqDuggYNEi1Qil1kpeCECfDQNjyMTK058ZBBhOWNMHBjlLWXUlRJDkRBBECY0vo4jRv22SvSaPUCAnkdJ9rbAp/kqb497PTIb2r1l1/ew8YdhINAlpYQFQezZVfkZdTKxt22n0QCjhupqjfh3gfNnbBX0z/iO+RvAOWRIZsjPFLC+jXl+n7cnu2cq1nvST5eHiYfXXeIgIwmeENLKqp+2Twr7PIdv22PnJkh6iR5kx7eTRxkNZdN quentin@deuxfleurs.fr"
    ];
    adrien = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINvUFN6HmZS5oxxOtmF6ug393m5NYbSbDI4G8pX6H9GZ adrien@pratchett"
    ];
    maximilien = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDHMMR6zNzz8NQU80wFquhUCeiXJuGphjP+zNouKbn228GyESu8sfNBwnuZq86vblR11Lz8l2rtCM73GfAKg29qmUWUHRKWvRIYWv2vaUJcCdy0bAxIzcvCvjZX0SpnIKxe9y3Rp0LGO5WLYfw0ZFaavwFZP0Z8w1Kj9/zBmL2X2avbhkaYHi/C1yXhbvESYQysmqLa48EX/TS616MBrgR9zbI9AoTQ9NOHnR14Tve/AP/khcZoBJdm4hTttMbNkEc0wonzdylTDew263SPRs/uoqnQIpUtErdPHqU10Yup8HjXjEyFJsSwcZcM5sZOw5JKckKJwmcd0yjO/x/4/Mk5 maximilien@icare"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGioTNbjGE3KblbqhnkEWUfGkYZ2p5UAVqPdQJaUBWoo maximilien@athena"
    ];
    trinity = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDWGWTRoF5MjQ5bmFdQENQlNdoYtA7Wd61GM0TMHZDki"
    ];
    baptiste = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnGkJZZrHIUp9q0DXmVLLuhCIe7Vu1J3j6dJ1z1BglqX7yOLdFQ6LhHXx65aND/KCOM1815tJSnaAyKWEj9qJ31RVUoRl42yBn54DvQumamJUaXAHqJrXhjwxfUkF9B73ZSUzHGADlQnxcBkmrjC5FkrpC/s4xr0o7/GIBkBdtZhX9YpxBfpH6wEcCruTOlm92E3HvvjpBb/wHsoxL1f2czvWe69021gqWEYRFjqtBwP36NYZnGOJZ0RrlP3wUrGCSHxOKW+2Su+tM6g07KPJn5l1wNJiOcyBQ0/Sv7ptCJ9+rTQNeVBMoXshaucYP/bKJbqH7dONrYDgz59C4+Kax"
    ];
    aeddis = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILoFf9fMYwLOpmiXKgn4Rs99YCj94SU1V0gwGXR5N4Md"
    ];
    boris = [
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPts/36UvMCFcx3anSMV8bQKGel4c4wCsdhDGWHzZHgg07DxMt+Wk9uv0hWkqLojkUbCl/bI5siftiEv6En0mHw="
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJaD6flgTLkKimMB1qukiLKLVqsN+gizgajETjTwbscXEP2Fajmqy+90v1eXTDcGivmTyi8wOqkJ0s4D7dWP7Ck="
      "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEIZKA/SIicXq7HPFJfumrMc1iARqA1TQWWuWLrguOlKgFPBVym/IVjtYGAQ/Xtv4wU9Ak0s+t9UKpQ/K38kVe0="
    ];
    vincent = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEve02acr522psrPxeElkwIPw2pc6QWtsUVZoaigqwZZ"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL/h+rxR2o+vN0hUWQPdpO7YY9aaKxO3ZRnUh9QiKBE7"
    ];
    armael = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJOoPghSM72AVp1zATgQzeLkuoGuP9uUTTAtwliyWoix"
    ];
    marion = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKzOhSTEOudBWCHi5wHc6MP0xjJJhuIDZEcx+hP6kz9N"
    ];
    darkgallium = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJX0A2P59or83EKhh32o8XumGz0ToTEsoq89hMbMtr7h"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB540H9kn+Ocs4Wjc1Y3f3OkHFYEqc5IM/FiCyoVVoh3"
    ];
    kokakiwi = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFPTsEgcOtb2bij+Ih8eg8ZqO7d3IMiWykv6deMzlSSS kokakiwi@kira"
    ];
    stitch = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILdT28Emp9yJqTPrxz+oDP08KZaN1kbsNyVqt9p9IMED"
    ];
  };

  # For Garage external communication
  networking.firewall.allowedTCPPorts = [ 3901 ];

  # All prod nodes were deployed on the same version.
  # This could be put in individual node .nix files if we deploy
  # newer nodes on a different system version, OR we can bump this
  # regularly cluster-wide
  system.stateVersion = "21.05";
}
