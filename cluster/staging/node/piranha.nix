# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "piranha";
  deuxfleurs.staticIPv4.address = "192.168.5.25";
  deuxfleurs.staticIPv6.address = "2001:912:1ac0:2200::25";

  # this denote the version at install time, do not update
  system.stateVersion = "24.05";
}
