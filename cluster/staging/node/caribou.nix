# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "caribou";
  deuxfleurs.staticIPv6.address = "2a01:e34:ec05:8a40::23";
  deuxfleurs.isRaftServer = true;

  # this denote the version at install time, do not update
  system.stateVersion = "21.05";
}
