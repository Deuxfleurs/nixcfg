# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot";
  boot.loader.timeout = 20;

  deuxfleurs.hostName = "df-pw5";
  deuxfleurs.staticIPv4.address = "192.168.5.130";
  deuxfleurs.staticIPv6.address = "2a02:a03f:6510:5102:223:24ff:feb0:e8a7";
  deuxfleurs.isRaftServer = true;

  # this denote the version at install time, do not update
  system.stateVersion = "24.05";
}
