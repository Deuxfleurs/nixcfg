job "dummy-nginx" {
  datacenters = ["neptune"]
  type = "service"
  
  group "nginx" {
    count = 1

    network {
      port "http" {
        to = 8080
      }
    }

    task "not-actually-nginx" {
      driver = "nix"
      config {
        packages = [
          "github:nixos/nixpkgs/nixos-21.05#python3",
        ]
        command = [ "/bin/python3", "-m", "http.server", "8080" ]
      }
    }

    service {
      port = "http"
      tags = [
        "tricot home.adnab.me 100",
      ]
      check {
        type = "http"
        path = "/"
        interval = "10s"
        timeout = "2s"
      }
    }
  }
}
