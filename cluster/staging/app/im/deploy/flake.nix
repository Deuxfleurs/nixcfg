{
  description = "Synapse packaging for Deuxfleurs";

  # nixpkgs 22.05 at 2022-11-29
  inputs.nixpkgs.url = "github:nixos/nixpkgs/fecf05d4861f3985e8dee73f08bc82668ef75125";

  outputs = { self, nixpkgs }:
  let
    # Generated with the help of pip2nix 0.8.0.dev1
    # See https://github.com/nix-community/pip2nix
    pypkgsOverlay = pkgs: self: super: {
      "pyyaml" = super.buildPythonPackage rec {
        pname = "pyyaml";
        version = "5.4.1";
        src = builtins.fetchurl {
          url = "https://files.pythonhosted.org/packages/a0/a4/d63f2d7597e1a4b55aa3b4d6c5b029991d3b824b5bd331af8d4ab1ed687d/PyYAML-5.4.1.tar.gz";
          sha256 = "0pm440pmpvgv5rbbnm8hk4qga5a292kvlm1bh3x2nwr8pb5p8xv0";
        };
        format = "setuptools";
        doCheck = false;
        buildInputs = [];
        checkInputs = [];
        nativeBuildInputs = [];
        propagatedBuildInputs = [];
      };
      "humanize" = super.buildPythonPackage rec {
        pname = "humanize";
        version = "0.5.1";
        src = builtins.fetchurl {
          url = "https://files.pythonhosted.org/packages/8c/e0/e512e4ac6d091fc990bbe13f9e0378f34cf6eecd1c6c268c9e598dcf5bb9/humanize-0.5.1.tar.gz";
          sha256 = "06dvhm3k8lf2rayn1gxbd46y0fy1db26m3h9vrq7rb1ib08mfgx4";
        };
        format = "setuptools";
        doCheck = false;
        buildInputs = [];
        checkInputs = [];
        nativeBuildInputs = [];
        propagatedBuildInputs = [];
      };
      "synapse-s3-storage-provider" = super.buildPythonPackage rec {
        pname = "synapse-s3-storage-provider";
        version = "1.1.2";
        src = builtins.fetchurl {
          url = "https://github.com/matrix-org/synapse-s3-storage-provider/archive/refs/tags/v1.1.2.zip";
          sha256 = "0xd5icfvnvdd3qadlsmqvj2qjm6rsvk1vbpiycdc7ypr9dp7x9z8";
        };
        format = "setuptools";
        doCheck = false;
        buildInputs = [];
        checkInputs = [];
        nativeBuildInputs = [
          pkgs."unzip"
        ];
        propagatedBuildInputs = [
          self."pyyaml"
          self."twisted"
          self."boto3"
          self."botocore"
          self."humanize"
          self."psycopg2"
          self."tqdm"
        ];
      };
    };
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      overlays = [
        (self: super: {
          python3 = super.python3.override {
            self = self.python3;
            packageOverrides = (pypkgsOverlay super);
          };
        })
      ];
    };

    synapse = pkgs.matrix-synapse.overridePythonAttrs (old: rec {
      propagatedBuildInputs = old.propagatedBuildInputs ++ [
        pkgs.python3.pkgs.synapse-s3-storage-provider
      ];
    });

    s3_provider = pkgs.python3.withPackages(ps: [ps.synapse-s3-storage-provider]);

    matrix_s3_async = pkgs.writeScriptBin "matrix-s3-async" ''
#!${pkgs.bash}/bin/bash

${pkgs.coreutils}/bin/cat > database.yaml <<EOF
user: ''$PG_USER
password: ''$PG_PASS
database: ''$PG_DB
host: ''$PG_HOST
port: ''$PG_PORT
EOF

while true; do
  ${s3_provider}/bin/s3_media_upload update-db 0d
  ${s3_provider}/bin/s3_media_upload --no-progress check-deleted ''$SYNAPSE_MEDIA_STORE
  ${s3_provider}/bin/s3_media_upload --no-progress upload ''$SYNAPSE_MEDIA_STORE ''$SYNAPSE_MEDIA_S3_BUCKET --delete --endpoint-url ''$S3_ENDPOINT
  ${pkgs.coreutils}/bin/sleep 600
done
      '';

    matrix_s3_async_sqlite = pkgs.writeScriptBin "matrix-s3-async-sqlite" ''
#!${pkgs.bash}/bin/bash

${pkgs.coreutils}/bin/cat > database.yaml <<EOF
sqlite:
  database: ''$SYNAPSE_SQLITE_DB
EOF

while true; do
  ${s3_provider}/bin/s3_media_upload update-db 0d
  ${s3_provider}/bin/s3_media_upload --no-progress check-deleted ''$SYNAPSE_MEDIA_STORE
  ${s3_provider}/bin/s3_media_upload --no-progress upload ''$SYNAPSE_MEDIA_STORE ''$SYNAPSE_MEDIA_S3_BUCKET --delete --endpoint-url ''$S3_ENDPOINT
  ${pkgs.coreutils}/bin/sleep 600
done
      '';

  in
  {
    packages.x86_64-linux = {
      inherit synapse s3_provider matrix_s3_async matrix_s3_async_sqlite;
    };
  };
}
