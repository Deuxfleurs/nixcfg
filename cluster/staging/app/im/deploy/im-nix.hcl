job "im" {
  datacenters = ["neptune"]
  type = "service"
  
  group "synapse" {
    count = 1

    network {
      port "http" {
        static = 8008
      }
    }

    ephemeral_disk {
      size    = 10000
    }

    restart {
      attempts = 10
      delay    = "30s"
    }

    task "restore-db" {
      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      driver = "nix2"
      config {
        packages = [
          "#litestream"
          ]
        command = "litestream"
        args = [
          "restore", "-config", "/etc/litestream.yml", "/ephemeral/homeserver.db"
        ]
        bind = {
          "../alloc/data" = "/ephemeral",
        }
      }

      template {
        data = file("../config/litestream.yml")
        destination = "etc/litestream.yml"
      }

      resources {
        memory = 100
        memory_max = 500
        cpu = 1000
      }
    }

    task "synapse" {
      driver = "nix2"
      config {
        nixpkgs = "github:nixos/nixpkgs/nixos-23.11"
        packages = [
          "#cacert",
          "#bash",
          "#coreutils",
          "#sqlite",
          ".#synapse",
        ]
        command = "synapse_homeserver"
        args = [
          "-n",
          "-c", "/etc/matrix-synapse/homeserver.yaml"
        ]
        bind = {
          "./secrets" = "/etc/matrix-synapse",
          "../alloc/data" = "/ephemeral",
        }
      }
      env = {
        SSL_CERT_FILE = "/etc/ssl/certs/ca-bundle.crt"
      }

      template {
        data = file("flake.nix")
        destination = "flake.nix"
      }
      template {
        data = file("flake.lock")
        destination = "flake.lock"
      }

      template {
        data = file("../config/homeserver.yaml")
        destination = "secrets/homeserver.yaml"
      }

      template {
        data = file("../config/synapse.log.config.yaml")
        destination = "secrets/synapse.log.config.yaml"
      }

      template {
        data = "{{ key \"secrets/synapse/signing_key\" }}"
        destination = "secrets/signing_key"
      }

      resources {
        memory = 2000
        memory_max = 3000
        cpu = 1000
      }

      service {
        port = "http"
        tags = [
          "tricot matrix.home.adnab.me 100",
          "tricot matrix.home.adnab.me:443 100",
          "tricot-add-header Access-Control-Allow-Origin *",
        ]
        check {
          type = "http"
          path = "/"
          interval = "10s"
          timeout = "2s"
        }
      }
    }

    task "media-async-upload" {
      driver = "nix2"

      config {
        packages = [
          "#bash",
          "#coreutils",
          ".#matrix_s3_async_sqlite",
        ]
        command = "sh"
        args = [
          "-c",
          "cd /ephemeral; matrix-s3-async-sqlite"
        ]
        bind = {
          "../alloc/data" = "/ephemeral",
        }
      }

      template {
        data = file("flake.nix")
        destination = "flake.nix"
      }
      template {
        data = file("flake.lock")
        destination = "flake.lock"
      }

      resources {
        cpu = 100
        memory = 100
        memory_max = 500
      }

      template {
        data = <<EOH
SYNAPSE_SQLITE_DB=/ephemeral/homeserver.db
SYNAPSE_MEDIA_STORE=/ephemeral/media_store
SYNAPSE_MEDIA_S3_BUCKET=synapse-data
AWS_ACCESS_KEY_ID={{ key "secrets/synapse/s3_access_key" | trimSpace }}
AWS_SECRET_ACCESS_KEY={{ key "secrets/synapse/s3_secret_key" | trimSpace }}
AWS_DEFAULT_REGION=garage-staging
S3_ENDPOINT=http://{{ env "attr.unique.network.ip-address" }}:3990
EOH
        destination = "secrets/env"
        env = true
      }
    }

    task "replicate-db" {
      driver = "nix2"
      config {
        packages = [
          "#litestream"
        ]
        command = "litestream"
        args = [
          "replicate", "-config", "/etc/litestream.yml"
        ]
        bind = {
          "../alloc/data" = "/ephemeral",
        }
      }

      template {
        data = file("../config/litestream.yml")
        destination = "etc/litestream.yml"
      }

      resources {
        memory = 500
        memory_max = 500
        cpu = 100
      }
    }
  }
}
