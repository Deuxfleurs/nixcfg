job "garage-staging" {
  datacenters = [ "neptune", "dathomir", "corrin", "bespin" ]
  type = "system"
  priority = 90


  update {
    max_parallel = 2
    min_healthy_time  = "60s"
  }

  group "garage-staging" {
    network {
      port "s3" { static = 3990 }
      port "rpc" { static = 3991 }
      port "web" { static = 3992 }
      port "k2v" { static = 3993 }
      port "admin" { static = 3909 }
    }

    update {
      max_parallel = 10
      min_healthy_time = "30s"
      healthy_deadline = "5m"
    }

    task "server" {
      driver = "docker"
      config {
        image = "dxflrs/garage:v1.99.0-internal"
        command = "/garage"
        args = [ "server" ]
        network_mode = "host"
        volumes = [
          "/mnt/storage/garage-staging/data:/data",
          "/mnt/ssd/garage-staging/meta:/meta",
          "secrets/garage.toml:/etc/garage.toml",
          "secrets:/etc/garage",
        ]
        logging {
          type = "journald"
        }
      }

      env = {
        RUST_LOG = "garage=info,garage_api=debug",
      }

      template {
        data = file("../config/garage.toml")
        destination = "secrets/garage.toml"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "secrets/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "secrets/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "secrets/consul-client.key"
      }

      resources {
        memory = 1500
        memory_max = 3000
        cpu = 500
      }

      kill_signal = "SIGINT"
      kill_timeout = "20s"

      restart {
        interval = "5m"
        attempts = 10
        delay    = "1m"
        mode     = "delay"
      }

      service {
        name = "garage-staging-rpc"
        tags = ["garage-staging-rpc"]
        port = "rpc"
      }

      #### Configuration for service ports: admin port (internal use only)

      service {
        name = "garage-staging-admin"
        tags = [
          "garage-staging-admin",
        ]
        port = "admin"
        check {
          name = "garage-tcp-liveness-check"
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      #### Configuration for service ports: externally available ports (S3 API, K2V, web)

      service {
        name = "garage-staging-s3-api"
        tags = [
          "garage-staging-api",
          "tricot garage.staging.deuxfleurs.org",
          "tricot *.garage.staging.deuxfleurs.org",
          "tricot-add-header Access-Control-Allow-Origin *",
          "tricot-on-demand-tls-ask http://garage-staging-admin.service.staging.consul:3909/check",
          "tricot-site-lb",
        ]
        port = "s3"
        # Check 1: Garage is alive and answering TCP connections
        check {
          name = "garage-staging-api-live"
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
        # Check 2: Garage is in a healthy state and requests should be routed here
        check {
          name = "garage-staging-api-healthy"
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }

      service {
        name = "garage-staging-k2v-api"
        tags = [
          "garage-staging-k2v-api",
          "tricot k2v.staging.deuxfleurs.org",
          "tricot-add-header Access-Control-Allow-Origin *",
          "tricot-site-lb",
        ]
        port = "k2v"
        # Check 1: Garage is alive and answering TCP connections
        check {
          name = "garage-staging-k2v-live"
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
        # Check 2: Garage is in a healthy state and requests should be routed here
        check {
          name = "garage-staging-k2v-healthy"
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }

      service {
        name = "garage-staging-web"
        tags = [
            "garage-staging-web",
            "tricot * 1",
            "tricot *.web.staging.deuxfleurs.org",
            "tricot staging.deuxfleurs.org",
            "tricot matrix.home.adnab.me/.well-known/matrix/server",
            "tricot-add-header Strict-Transport-Security max-age=63072000; includeSubDomains; preload",
            "tricot-add-header X-Frame-Options SAMEORIGIN",
            "tricot-add-header X-XSS-Protection 1; mode=block",
            "tricot-add-header X-Content-Type-Options nosniff",
            "tricot-add-header Access-Control-Allow-Origin *",
            "tricot-on-demand-tls-ask http://garage-staging-admin.service.staging.consul:3909/check",
            "tricot-site-lb",
        ]
        port = "web"
        # Check 1: Garage is alive and answering TCP connections
        check {
          name = "garage-staging-web-live"
          type = "tcp"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
        # Check 2: Garage is in a healthy state and requests should be routed here
        check {
          name = "garage-staging-web-healthy"
          port = "admin"
          type = "http"
          path = "/health"
          interval = "60s"
          timeout = "5s"
        }
      }
    }
  }
}
