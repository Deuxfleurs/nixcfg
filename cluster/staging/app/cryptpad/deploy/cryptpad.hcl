job "cryptpad" {
  datacenters = ["neptune"]
  type = "service"

  group "cryptpad" {
    count = 1

    network {
      port "http" {
        to = 3000
      }
    }

    restart {
      attempts = 10
      delay    = "30s"
    }

    task "main" {
      driver = "docker"

      constraint {
        attribute = "${attr.unique.hostname}"
        operator = "="
        value = "caribou"
      }

      config {
        image = "armael/cryptpad:2024.12.0"
        ports = [ "http" ]

        volumes = [
          "/mnt/ssd/cryptpad:/mnt",
          "secrets/config.js:/cryptpad/config.js",
        ]
      }
      env {
        CRYPTPAD_CONFIG = "/cryptpad/config.js"
      }

      template {
        data = file("../config/config.js")
        destination = "secrets/config.js"
      }

      /* Disabled because it requires modifications to the docker image and I do not want to invest the time yet
      template {
        data = file("../config/application_config.js")
        destination = "secrets/config.js"
      }
      */

      resources {
        memory = 1000
        cpu = 500
      }

      service {
        name = "cryptpad"
        port = "http"
        tags = [
          "tricot pad.staging.deuxfleurs.org",
          "tricot pad-sandbox.staging.deuxfleurs.org",
          "tricot-add-header Cross-Origin-Resource-Policy cross-origin",
          "tricot-add-header Cross-Origin-Embedder-Policy require-corp",
          "tricot-add-header Access-Control-Allow-Origin *",
          "tricot-add-header Access-Control-Allow-Credentials true",
          "d53-cname pad.staging.deuxfleurs.org",
          "d53-cname pad-sandbox.staging.deuxfleurs.org",
        ]
        check {
          type = "http"
          path = "/"
          interval = "10s"
          timeout = "2s"
        }
      }
    }
  }
}
