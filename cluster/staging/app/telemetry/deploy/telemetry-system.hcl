job "telemetry-system" {
  datacenters = ["neptune", "dathomir", "corrin", "bespin"]
  type = "system"
  priority = "100"

  group "collector" {
     network {
       port "node_exporter" { static = 9100 }
     }

		task "node_exporter" {
			driver = "docker"

			config {
				image = "quay.io/prometheus/node-exporter:v1.8.1"
				network_mode = "host"
				volumes = [
					"/:/host:ro,rslave"
				]
				args = [ "--path.rootfs=/host" ]
			}

			resources {
				cpu = 50
				memory = 40
			}

       service {
         tags = [ "telemetry" ]
         port = 9100
         address_mode = "driver"
         name = "node-exporter"
         check {
           type = "http"
           path = "/"
           port = 9100
           address_mode = "driver"
           interval = "60s"
           timeout = "5s"
           check_restart {
             limit = 3
             grace = "90s"
             ignore_warnings = false
           }
         }
       }
		}
	}
 }
