job "telemetry-storage" {
  datacenters = ["neptune", "dathomir", "corrin", "bespin"]
  type = "service"

  group "prometheus" {
    count = 2

    network {
      port "prometheus" {
        static = 9090
      }
    }

    constraint {
      attribute = "${attr.unique.hostname}"
      operator = "set_contains_any"
      value = "df-pw5,origan"
    }

    task "prometheus" {
      driver = "docker"
      config {
        image = "prom/prometheus:v3.1.0"
        network_mode = "host"
        ports = [ "prometheus" ]
        args = [
          "--config.file=/etc/prometheus/prometheus.yml",
          "--storage.tsdb.path=/data",
          "--storage.tsdb.retention.size=20GB",
        ]
        volumes = [
          "secrets:/etc/prometheus",
          "/mnt/ssd/prometheus:/data"
        ]
      }

      template {
        data = file("../config/prometheus.yml")
        destination = "secrets/prometheus.yml"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "secrets/consul.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "secrets/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "secrets/consul-client.key"
      }

      template {
        data = "{{ key \"secrets/nomad/nomad-ca.crt\" }}"
        destination = "secrets/nomad-ca.crt"
      }

      template {
        data = "{{ key \"secrets/nomad/nomad-client.crt\" }}"
        destination = "secrets/nomad-client.crt"
      }

      template {
        data = "{{ key \"secrets/nomad/nomad-client.key\" }}"
        destination = "secrets/nomad-client.key"
      }

      resources {
        memory = 500
        cpu = 200
      }

      service {
        port = 9090
        address_mode = "driver"
        name = "prometheus"
        check {
          type = "http"
          path = "/"
          port = 9090
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}
