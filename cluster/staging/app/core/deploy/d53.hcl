job "core-d53" {
  datacenters = ["neptune", "dathomir", "corrin", "bespin"]
  type = "service"
  priority = 90

  group "D53" {
    count = 1

    task "d53" {
      driver = "docker"

      config {
        image = "lxpz/amd64_d53:4"
        network_mode = "host"
        readonly_rootfs = true
        volumes = [
          "secrets:/etc/d53",
        ]
      }

      resources {
        cpu = 100
        memory = 100
      }

      restart {
        interval = "3m"
        attempts = 10
        delay    = "15s"
        mode     = "delay"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "secrets/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "secrets/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "secrets/consul-client.key"
      }

      template {
        data = <<EOH
D53_CONSUL_HOST=https://localhost:8501
D53_CONSUL_CA_CERT=/etc/d53/consul-ca.crt
D53_CONSUL_CLIENT_CERT=/etc/d53/consul-client.crt
D53_CONSUL_CLIENT_KEY=/etc/d53/consul-client.key
D53_PROVIDERS=deuxfleurs.org:gandi
D53_GANDI_API_KEY={{ key "secrets/d53/gandi_api_key" }}
D53_ALLOWED_DOMAINS=staging.deuxfleurs.org
RUST_LOG=d53=debug
EOH
        destination = "secrets/env"
        env = true
      }
    }
  }
}
