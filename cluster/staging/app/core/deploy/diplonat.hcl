job "core-diplonat" {
  datacenters = ["neptune", "dathomir", "corrin", "bespin"]
  type = "system"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  update {
    max_parallel     = 3
    stagger = "20s"
  }

  group "diplonat" {
    task "diplonat" {
      driver = "docker"

      config {
        image = "lxpz/amd64_diplonat:7"
        network_mode = "host"
        readonly_rootfs = true
        privileged = true
        volumes = [
          "secrets:/etc/diplonat",
        ]
      }

      restart {
        interval = "30m"
        attempts = 2
        delay    = "15s"
        mode     = "delay"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "secrets/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "secrets/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "secrets/consul-client.key"
      }

      template {
        data = <<EOH
DIPLONAT_REFRESH_TIME=60
DIPLONAT_EXPIRATION_TIME=300
DIPLONAT_IPV6_ONLY=true
DIPLONAT_CONSUL_NODE_NAME={{ env "attr.unique.hostname" }}
DIPLONAT_CONSUL_URL=https://localhost:8501
DIPLONAT_CONSUL_CA_CERT=/etc/diplonat/consul-ca.crt
DIPLONAT_CONSUL_CLIENT_CERT=/etc/diplonat/consul-client.crt
DIPLONAT_CONSUL_CLIENT_KEY=/etc/diplonat/consul-client.key
RUST_LOG=debug
RUST_BACKTRACE=1
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 100
      }
    }
  }
}
