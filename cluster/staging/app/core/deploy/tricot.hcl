job "core-tricot" {
  datacenters = ["neptune", "dathomir", "corrin", "bespin"]
  type = "system"
  priority = 90

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  update {
    max_parallel     = 1
    stagger = "1m"
  }

  group "tricot" {
    network {
      port "http_port" { static = 80 }
      port "https_port" { static = 443 }
      port "metrics_port" { static = 9334 }
    }

    task "server" {
      driver = "docker"

      config {
        image = "armael/tricot:40g7jpp915jkfszlczfh1yw2x6syjkxs-redir-headers"
        network_mode = "host"
        readonly_rootfs = true
        ports = [ "http_port", "https_port" ]
        volumes = [
          "secrets:/etc/tricot",
        ]
        ulimit {
          nofile = "65535:65535"
        }
      }

      resources {
        cpu = 500
        memory = 200
        memory_max = 500
      }

      restart {
        interval = "30m"
        attempts = 2
        delay    = "15s"
        mode     = "delay"
      }

      template {
        data = "{{ key \"secrets/consul/consul-ca.crt\" }}"
        destination = "secrets/consul-ca.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.crt\" }}"
        destination = "secrets/consul-client.crt"
      }

      template {
        data = "{{ key \"secrets/consul/consul-client.key\" }}"
        destination = "secrets/consul-client.key"
      }

      template {
        data = <<EOH
TRICOT_NODE_NAME={{ env "attr.unique.consul.name" }}
TRICOT_LETSENCRYPT_EMAIL=alex@adnab.me
#TRICOT_ENABLE_COMPRESSION=true
TRICOT_CONSUL_HOST=https://localhost:8501
TRICOT_CONSUL_CA_CERT=/etc/tricot/consul-ca.crt
TRICOT_CONSUL_CLIENT_CERT=/etc/tricot/consul-client.crt
TRICOT_CONSUL_CLIENT_KEY=/etc/tricot/consul-client.key
TRICOT_HTTP_BIND_ADDR=[::]:80
TRICOT_HTTPS_BIND_ADDR=[::]:443
TRICOT_METRICS_BIND_ADDR=[::]:9334
TRICOT_WARMUP_CERT_MEMORY_STORE=true
RUST_LOG=tricot=trace
RUST_BACKTRACE=1
EOH
        destination = "secrets/env"
        env = true
      }

      service {
        name = "tricot-http"
        port = "http_port"
        tags = [
          "(diplonat (tcp_port 80))"
        ]
        address_mode = "host"
      }

      service {
        name = "tricot-https"
        port = "https_port"
        tags = [
          "(diplonat (tcp_port 443))",
          "d53-aaaa ${attr.unique.hostname}.machine.staging.deuxfleurs.org",
          "d53-aaaa ${meta.site}.site.staging.deuxfleurs.org",
          "d53-aaaa staging.deuxfleurs.org"
        ]
        address_mode = "host"
      }

      service {
        name = "tricot-metrics"
        port = "metrics_port"
        address_mode = "host"
      }
    }
  }
}
