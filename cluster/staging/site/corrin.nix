{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "corrin";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.5.1";
  deuxfleurs.cnameTarget = "corrin.site.staging.deuxfleurs.org.";
  deuxfleurs.publicIPv4 = "45.81.62.36";
}
