{ ... }:
{
  deuxfleurs.siteName = "dathomir";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.1.1";
  deuxfleurs.cnameTarget = "dathomir.site.staging.deuxfleurs.org.";
}
