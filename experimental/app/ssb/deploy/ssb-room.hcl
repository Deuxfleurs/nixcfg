job "ssb" {
  datacenters = ["neptune"]
  type = "service"

  group "ssb-room" {
    count = 1

    constraint {
      attribute = "${attr.unique.hostname}"
      value     = "caribou"
    }

    network {
      port "web_port" {
        to = 8007
      }
      port "ssb_port" {
        to = 8008
      }
    }

    task "go-ssb-room" {
      driver = "docker"
      config {
        image = "lxpz/amd64_ssb_room:3"
        readonly_rootfs = true
        ports = [ "web_port", "ssb_port" ]
        network_mode = "host"
        volumes = [
          "/mnt/ssd/ssb-room:/root/.ssb/"
        ]
      }
      user = "root"

      resources {
        memory = 200
      }

      service {
        name = "ssb-room-http"
        tags = [
          "tricot ssb.staging.deuxfleurs.org",
        ]
        port = "web_port"
        address_mode = "driver"
      }

      service {
        name = "ssb-room-ssb"
        tags = [
          "(diplonat (port 8008))",
        ]
        port = "ssb_port"
        address_mode = "driver"
      }
    }
  }
}

