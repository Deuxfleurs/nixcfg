job "nextcloud" {
  datacenters = ["neptune"]
  type = "service"
  
  group "nextcloud" {
    count = 1

    network {
      port "http" {
        to = 80
      }
    }

    ephemeral_disk {
      size    = 10000
    }

    restart {
      attempts = 10
      delay    = "30s"
    }

    task "restore-db" {
      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      driver = "docker"
      config {
        image = "litestream/litestream:0.3.7"
        args = [
          "restore", "-config", "/etc/litestream.yml", "/ephemeral/nextcloud.db"
        ]
        volumes = [
          "../alloc/data:/ephemeral",
          "secrets/litestream.yml:/etc/litestream.yml"
        ]
      }
      user = "33"

      template {
        data = file("../config/litestream.yml")
        destination = "secrets/litestream.yml"
      }

      resources {
        memory = 200
        cpu = 1000
      }
    }

    task "nextcloud" {
      driver = "docker"
      config {
        image = "nextcloud:22.2.3-apache"
        ports = [ "http" ]
        #entrypoint = [ "/bin/sh", "-c" ]
        #command = "apache2-foreground"

        volumes = [
          "../alloc/data:/var/www/html/data",
        ]
      }
      user = "33"

      template {
        data = <<EOH
SQLITE_DATABASE=nextcloud
NEXTCLOUD_ADMIN_USER={{ key "secrets/nextcloud/admin_user" }}
NEXTCLOUD_ADMIN_PASSWORD={{ key "secrets/nextcloud/admin_pass" }}
NEXTCLOUD_TRUSTED_DOMAINS=cloud.home.adnab.me
OVERWRITEHOST=cloud.home.adnab.me
OVERWRITEPROTOCOL=https
OBJECTSTORE_S3_HOST={{ env "attr.unique.network.ip-address" }}
OBJECTSTORE_S3_PORT=3990
OBJECTSTORE_S3_BUCKET=nextcloud-data
OBJECTSTORE_S3_KEY={{ key "secrets/nextcloud/s3_access_key" }}
OBJECTSTORE_S3_SECRET={{ key "secrets/nextcloud/s3_secret_key" }}
OBJECTSTORE_S3_SSL=false
OBJECTSTORE_S3_REGION=garage-staging
OBJECTSTORE_S3_USEPATH_STYLE=true
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 2500
        cpu = 1000
      }

      service {
        port = "http"
        tags = [
          "tricot cloud.home.adnab.me 100",
        ]
        check {
          type = "tcp"
          port = "http"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }

    task "replicate-db" {
      driver = "docker"
      config {
        image = "litestream/litestream:0.3.7"
        args = [
          "replicate", "-config", "/etc/litestream.yml"
        ]
        volumes = [
          "../alloc/data:/ephemeral",
          "secrets/litestream.yml:/etc/litestream.yml"
        ]
      }
      user = "33"

      template {
        data = file("../config/litestream.yml")
        destination = "secrets/litestream.yml"
      }

      resources {
        memory = 200
        cpu = 100
      }
    }
  }
}
