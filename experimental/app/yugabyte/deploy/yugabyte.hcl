job "yugabytedb" {
  type = "service"
  datacenters = [ "neptune", "pluton" ]

  priority = 80

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "master" {
    count = 3

    constraint {
      attribute = "${attr.unique.hostname}"
      operator  = "regexp"
      value     = "(caribou|cariacou|carcajou)"
    }

    network {
      port "admin" { static = 7000 }
      port "master-rpc" { static = 7100 }
    }

    update {
      max_parallel = 1
      min_healthy_time = "30s"
      healthy_deadline = "5m"
    }

    task "master" {
      driver = "docker"

      config {
        image = "yugabytedb/yugabyte:2.11.1.0-b305"
        command = "/home/yugabyte/bin/yb-master"
        args = [ 
          "--fs_data_dirs=/mnt/master",
          "--replication_factor=3",
          "--master_addresses=10.42.0.21:7100,10.42.0.22:7100,10.42.0.23:7100",
          "--rpc_bind_addresses=0.0.0.0:7100",
          "--placement_cloud=deuxfleurs",
          "--placement_region=staging",
          "--placement_zone=neptune",
          "--minloglevel=1",
        ]
        volumes = [
          "/mnt/ssd/yugabyte/master:/mnt/master",
        ]
        network_mode = "host"
        logging {
          type = "journald"
        }
      }

      resources {
        memory = 1000
        cpu = 1000
      }

      kill_signal = "SIGINT"
      kill_timeout = "20s"

      service {
        tags = ["yugabyte-master-rpc"]
        port = 7100
        address_mode = "driver"
        name = "yugabyte-master-rpc"
        check {
          type = "tcp"
          port = 7100
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = ["yugabyte-admin"]
        port = 7000
        address_mode = "driver"
        name = "yugabyte-admin"
        check {
          type = "tcp"
          port = 7000
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      restart { 
        interval = "30m"  
        attempts = 10  
        delay    = "15s"  
        mode     = "delay"
      }
    }
  }

  group "tserver" {
    count = 3

    constraint {
      attribute = "${attr.unique.hostname}"
      operator  = "!="
      value     = "spoutnik"
    }

    network {
      port "ysql" { static = 5433 }
      port "tserver-rpc" { static = 9100 }
    }

    update {
      max_parallel = 1
      min_healthy_time = "30s"
      healthy_deadline = "5m"
    }

    task "tserver" {
      driver = "docker"

      config {
        image = "yugabytedb/yugabyte:2.11.1.0-b305"
        command = "/home/yugabyte/bin/yb-tserver"
        args = [ 
          "--fs_data_dirs=/mnt/tserver",
          "--start_pgsql_proxy",
          "--tserver_master_addrs=10.42.0.21:7100,10.42.0.22:7100,10.42.0.23:7100",
          "--rpc_bind_addresses=0.0.0.0:9100",
          "--placement_cloud=deuxfleurs",
          "--placement_region=staging",
          "--placement_zone=neptune",
          "--minloglevel=1",
        ]
        volumes = [
          "/mnt/ssd/yugabyte/tserver:/mnt/tserver",
        ]
        network_mode = "host"
        logging {
          type = "journald"
        }
      }

      resources {
        memory = 1000
        cpu = 1000
      }

      kill_signal = "SIGINT"
      kill_timeout = "20s"

      service {
        tags = ["yugabyte-tserver-rpc"]
        port = 9100
        address_mode = "driver"
        name = "yugabyte-tserver-rpc"
        check {
          type = "tcp"
          port = 9100
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }

      service {
        tags = [ "yugabyte-ysql" ]
        port = 5433
        address_mode = "driver"
        name = "yugabyte-ysql"
        check {
          type = "tcp"
          port = 5433
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
}
