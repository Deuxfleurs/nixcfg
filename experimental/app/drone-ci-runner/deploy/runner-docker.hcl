job "drone-runner" {
  datacenters = ["neptune"]
  type = "system"

  group "runner" {

    task "populate-nix-store" {
      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      driver = "docker"
      config {
        image = "nixpkgs/nix:nixos-21.05"
        command = "sh"
        args = [
          "-c", "test -d /mnt/store || cp -rv /nix/{store,var} /mnt/"
        ]
        volumes = [
          "/var/lib/drone/nix:/mnt",
        ]
      }

      resources {
        memory = 100
        cpu = 100
      }
    }

    task "drone-runner" {
      driver = "docker"
      config {
        image = "drone/drone-runner-docker:1.8.1"

        volumes = [
          "/var/lib/drone/nix:/nix",
          "/var/run/docker.sock:/var/run/docker.sock"
        ]
      }

      template {
        data = <<EOH
DRONE_RPC_PROTO=https
DRONE_RPC_HOST=drone.deuxfleurs.fr
DRONE_RPC_SECRET={{ key "secrets/drone-ci/rpc_secret" | trimSpace }}
DRONE_RUNNER_CAPACITY=1
DRONE_DEBUG=true
DRONE_LOGS_TRACE=true
DRONE_RPC_DUMP_HTTP=true
DRONE_RPC_DUMP_HTTP_BODY=true
DRONE_RUNNER_NAME={{ env "attr.unique.hostname" }}
DRONE_RUNNER_LABELS=nix:1
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 200
        cpu = 100
      }
    }
  
    task "drone-gc" {
      driver = "docker"
      config {
        image = "drone/gc:latest"

        volumes = [
          "/var/run/docker.sock:/var/run/docker.sock"
        ]
      }

      template {
        data = <<EOH
GC_DEBUG=true
GC_CACHE=10gb
GC_INTERVAL=10m
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 100
        cpu = 100
      }
    }
  }
}
